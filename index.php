<?php
function wiki($person) {
    $url = "https://en.wikipedia.org/w/api.php?action=opensearch&search="
            .urlencode($person)."&format=xml&limit=1";
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_NOBODY, false);
    curl_setopt($ch, CURLOPT_VERBOSE, FALSE);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($ch, CURLOPT_MAXREDIRS, 4);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    $page = curl_exec($ch);
    $xml = simplexml_load_string($page);
    if((string)$xml->Section->Item->Description) {
        $description =  (string)$xml->Section->Item->Description;
        preg_match('/\d{4}/', $description, $matches);
        $birthyear = empty($matches)? $birthyear = "NOT FOUND" : min($matches);
        return '<b>'.$person.'</b>' ." was born on: ".$birthyear;
    } else {
        return "birthyear not found";
    }
}

//example
echo wiki('einstein').'<br>' ;
echo wiki('sunny leone').'<br>' ;
echo wiki('naila nayem').'<br>' ;
echo wiki('thomas alva edison').'<br>' ;
echo wiki('mark zuckerberg').'<br>' ;
echo wiki('sheikh mujib').'<br>' ;
echo wiki('mahatma gandhi').'<br>' ;
